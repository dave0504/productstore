﻿using ProductStore.Models;
using ProductStore.Web.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductStore.DAL.Repositories
{
    public class ProductRepository : GenericRepository<Account>
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
            _context = context;
        }

    }
}
