﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ProductStore.Models;
using ProductStore.Web.DAL;

namespace ProductStore.DAL.Repositories
{
    public class CustomerRepository : GenericRepository<Customer>
    {
        public CustomerRepository(DatabaseContext context) : base(context)
        {
            _context = context;
        }
    }
}
