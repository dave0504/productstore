﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using ProductStore.Models;
using ProductStore.Web.DAL;

namespace ProductStore.DAL.Repositories
{
    public class AccountRepository : GenericRepository<Account>
    {
        
        public AccountRepository(DatabaseContext context) : base(context)
        {
            _context = context;
        }

        /// <summary>
        /// Finds customer by email address.
        /// </summary>
        /// <param name="email">The customers email address.</param>
        /// <returns>Customer Entity if found, otherwise null.</returns>
        public Account FindAccountByEmailAddress(string email)
        {
            email = email.ToLower();
            //var customer = (from a in _context.Account where a.Email == email select a).SingleOrDefaultAsync();
            var account = _context.Account.Where(x => x.Email.Equals(email, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
            return account;
        }

        /// <summary>
        /// Finds customer by username.
        /// </summary>
        /// <param name="email">The customers email address.</param>
        /// <returns>Customer Entity if found, otherwise null.</returns>
        public Account FindAccountByUsername(string username)
        {
            var account = _context.Account.Where(x => x.Username.Equals(username, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
            return account;
        }
    }
}
