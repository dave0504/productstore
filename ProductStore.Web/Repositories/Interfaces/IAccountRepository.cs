﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Repositories.Interfaces
{
    public interface IAccountRepository<Account> : IRepository<Account>
    {
        Account FindAccountByEmailAddress(string email);
        Account FindAccountByUsername(string username);
    }
}
