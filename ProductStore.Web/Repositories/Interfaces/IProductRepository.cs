﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductStore.Repositories.Interfaces
{
    public interface IProductRepository<Product> : IRepository<Product>
    {

    }
}
