﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Repositories.Interfaces
{
    public interface ICustomerRepository<Customer> : IRepository<Customer>
    {
        
    }
}
