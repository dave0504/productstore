﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Repositories.Interfaces
{
    public interface IRepository<TEntity>
    {
        void Insert(TEntity t);
        void Update(TEntity t);
        void Delete(object Id);
        void Delete(TEntity entityToDelete);
        TEntity FindById(int Id);
    }
}