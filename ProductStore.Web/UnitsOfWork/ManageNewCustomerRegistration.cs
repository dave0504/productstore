﻿using ProductStore.DAL.Repositories;
using ProductStore.Models;
using ProductStore.Web.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace ProductStore.Web.UnitsOfWork
{
    public class ManageNewCustomerRegistration : UnitOfWork
    {
        public ManageNewCustomerRegistration(DatabaseContext context) : base(context)
        {

        }

        internal bool ProcessNewCustomerSignUp(Customer customer, Address address, Account account)
        {
            try
            {
                CustomerRepository.Insert(customer);
                //AcountRepository.Insert(account);
                AddressRepository.Insert(address);
                CustomerAddressesRepository.Insert(new CustomerAddresses
                {
                    CustomerID = customer.ID,
                    AddressID = address.ID,
                    DateFrom = DateTime.Now.Date,

                });
                Save();
            }
            catch (Exception ex) when (ex is DbUpdateException || ex is DbUpdateConcurrencyException)
            {
                return false;
            }

            return true;
        }
    }
}