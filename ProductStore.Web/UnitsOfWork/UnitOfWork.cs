﻿using ProductStore.DAL.Repositories;
using ProductStore.Models;
using ProductStore.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductStore.Web.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DatabaseContext _context;
        private AccountRepository accountRepository;
        private GenericRepository<Address> addressRepository;
        private GenericRepository<CustomerAddresses> customerAddressRepository;
        private GenericRepository<Customer> customerRepository;
        private ProductRepository productRepository;

        public UnitOfWork(DatabaseContext context)
        {
            _context = context;
        }

        public AccountRepository AcountRepository
        {
            get
            {
                return this.accountRepository ?? new AccountRepository(_context);
            }
        }

        public GenericRepository<Address> AddressRepository
        {
            get
            {
                return this.addressRepository ?? new GenericRepository<Address>(_context);
            }
        }

        public GenericRepository<CustomerAddresses> CustomerAddressesRepository
        {
            get
            {
                return this.customerAddressRepository ?? new GenericRepository<CustomerAddresses>(_context);
            }
        }

        public GenericRepository<Customer> CustomerRepository
        {
            get
            {
                return this.customerRepository ?? new GenericRepository<Customer>(_context);
            }
        }

        public ProductRepository ProductRepository
        {
            get
            {
                return this.productRepository ?? new ProductRepository(_context);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}