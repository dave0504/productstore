﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProductStore.Web.DAL
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Account> Account { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerAddresses> CustomerAddresses { get; set; }
        public DbSet<Delivery> Delivery { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductPrices> ProductPrices { get; set; }
        public DbSet<Supplier> Supplier { get; set; }
        public DbSet<SupplierAddresses> SupplierAddresses { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().ToTable("Accounts");
            modelBuilder.Entity<Address>().ToTable("Addresses");
            modelBuilder.Entity<Customer>().ToTable("Customers");
            modelBuilder.Entity<CustomerAddresses>().ToTable("CustomerAddresses").HasKey(k => new { k.CustomerID, k.AddressID, k.DateFrom });
            modelBuilder.Entity<Delivery>().ToTable("Deliveries");
            modelBuilder.Entity<Order>().ToTable("Orders");
            modelBuilder.Entity<OrderItem>().ToTable("OrderItems").HasKey(k => new { k.OrderID, k.ProductID });
            modelBuilder.Entity<Payment>().ToTable("Payments");
            modelBuilder.Entity<PaymentMethod>().ToTable("PaymentMethods");
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<ProductPrices>().ToTable("ProductPrices").HasKey(k => new { k.ProductID, k.FromDate });
            modelBuilder.Entity<Supplier>().ToTable("Suppliers");
            modelBuilder.Entity<SupplierAddresses>().ToTable("SupplierAddresses").HasKey(k => new { k.SupplierID, k.AddressID, k.DateFrom });

            // Set relations and stop cascade on delete

            modelBuilder.Entity<Customer>().HasRequired(x => x.Account);
            modelBuilder.Entity<Order>().HasRequired(x => x.Payment);
            modelBuilder.Entity<PaymentMethod>().HasRequired(x => x.Customer).WithMany(x => x.PaymentMethod).WillCascadeOnDelete(false);
        }
    }
}