﻿using ProductStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace ProductStore.Web.DAL
{
    public class DatabaseInitializer : IDatabaseInitializer<DatabaseContext>
    {
        public void InitializeDatabase(DatabaseContext context)
        {
            Seed(context);
        }

        public static void Seed(DatabaseContext context)
        {
            context.Database.CreateIfNotExists();
            List<Supplier> suppliers = new List<Supplier>();

            if (!context.Supplier.Any())
            {
                suppliers = new List<Supplier>
                {
                    new Supplier()
                    {
                        Name = "Outdoor World",
                        PhoneOne = "01245786598",
                        PhoneTwo = "01245786512",
                        Email = "supplies@outdoorworld.co.uk"
                    },
                    new Supplier()
                    {
                        Name = "ForeMore Golf",
                        PhoneOne = "01325649787",
                        PhoneTwo = "01325649745",
                        Email = "orders@foremoregolf.co.uk"
                    },
                    new Supplier()
                    {
                        Name = "Tough Climb",
                        PhoneOne = "02653424587",
                        PhoneTwo = "02653424544",
                        Email = "supplies@toughclimb.com"
                    },
                    new Supplier()
                    {
                        Name = "Safeline",
                        PhoneOne = "03458456124",
                        PhoneTwo = "03458454587",
                        Email = "orders@safeline.co.uk"
                    },
                    new Supplier()
                    {
                        Name = "Farsight",
                        PhoneOne = "01879465312",
                        PhoneTwo = "01879465344",
                        Email = "supplies@farsight.com"
                    },
                    new Supplier()
                    {
                        Name = "Perzonalise",
                        PhoneOne = "01645236996",
                        PhoneTwo = "01645236978",
                        Email = "supplies@perzonalise.com"
                    }
                };
                context.Supplier.AddRange(suppliers);
                context.SaveChanges();
            }

            if (!context.Product.Any())
            {
                var lines = File.ReadLines(@"C:\repositories\ProductStore\ProductStore.Web\App_Data\products.csv").Select(a => a.Split(','));
                foreach (var line in lines)
                {
                    var name = line[0];
                    Product p = new Product()
                    {
                        Name = line[4],
                        Description = line[7],
                        ProductLine = line[2],
                        ProductType = line[3],
                        Price = Math.Round(Convert.ToDecimal(line[6]), 2),
                        StockLevel = Convert.ToInt32(line[5]),
                        SupplierID = context.Supplier.Where(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase)).SingleOrDefault().ID
                    };
                    context.Product.Add(p);
                        
                }
            }

            foreach (Product product in context.Product)
            {
                product.Description = product.Description.Replace('"', ' ').TrimStart();
            }
            context.SaveChanges();
        }
    }
}