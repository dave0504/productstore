﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ProductStore.Models
{
    public class Customer
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string Firstname { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        [Required]
        public string Surname { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DOB { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneOne { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneTwo { get; set; }

        public Account Account { get; set; }
        public ICollection<Order> Orders { get; set; }
        public ICollection<CustomerAddresses> CustomerAddresses { get; set; }
        public ICollection<Payment> Payment { get; set; }
        public ICollection<PaymentMethod> PaymentMethod { get; set; }

    }

    public class Gender
    {
        public const string Male = "male";
        public const string Female = "female";
    }
}
