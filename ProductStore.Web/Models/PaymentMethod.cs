﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class PaymentMethod
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        
        [DataType(DataType.CreditCard)]
        [Required]
        public string CardNumber { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime FromDate { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime ToDate { get; set; }

        [Required]
        [StringLength(100)]
        public string CardHolderName { get; set; }

        public int? IssueNumber { get; set; }
        public Customer Customer { get; set; }

        public ICollection<Payment> Payment { get; set; }
    }
}
