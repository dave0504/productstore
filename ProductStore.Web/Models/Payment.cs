﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProductStore.Models
{
    public class Payment
    {
        public int ID { get; set; }

        [Required]
        public int CustomerID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public int PaymentMethodID { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }

        [Required]
        public string VendorTxCode { get; set; }


        

        public Customer Customer { get; set; }
        public Order Order { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Merchant Merchant { get; set; }

    }

    public enum Merchant
    {
        SagePay, PayPal
    }
}