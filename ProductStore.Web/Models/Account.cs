﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProductStore.Models
{
    public class Account
    {
        public int ID { get; set; }

        [StringLength(20)]
        [Required]
        public string Username { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [Required]
        public int CustomerID { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime RegistrationDate { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Required]
        public string AccountStatus { get; set; }

        public Customer Customer { get; set; }
    }


    public class AccountStatus
    {
        public const string Active = "active";
        public const string Closed = "closed";
        public const string Suspended = "suspended";
    };
}
