﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductStore.Models
{
    public class Delivery
    {
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DeliveryCreated { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Dispatched { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Delivered { get; set; }

        [Required]
        public DeliveryStatus DeliveryStatus { get; set; }

        public Order Order { get; set; }
        public ICollection<OrderItem> OrderItem  { get; set;}

    }

    public enum DeliveryStatus
    {
        Warehouse, Delivered, Transit, AwaitingDispatch, Posted
    }
}