﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Models
{

    public class Product
    {
        public int ID { get; set; }

        public int? ParentProductID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string ProductLine { get; set; }

        [Required]
        public string ProductType { get; set; }


        [Required]
        public int SupplierID { get; set; }

        [DataType(DataType.Currency)]
        [Required]
        public decimal Price { get; set; }

        [Required]
        public int StockLevel { get; set; }

        public ICollection<ProductPrices> ProductPrices { get; set; }
        public ICollection<OrderItem> OrderItem { get; set; }
        public Supplier Supplier { get; set; }

        public Product ParentProduct { get; set; }
        public ICollection<Product> ChildProducts { get; set; }


    }
}
