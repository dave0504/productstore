﻿using ProductStore.Web.Models.AttributeValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ProductStore.Web.Models
{
    public class SignUpModel
    {
        [Display(Name = "Username")]
        [StringLength(20)]
        [Required(ErrorMessage = "*")]
        [Remote("IsUsernameAvailable", "SignUp")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "*")]
        public string PasswordConfirm { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string Firstname { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string Surname { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "*")]
        [CustomDateAttribute]
        public DateTime DOB { get; set; }

        [Required]
        public string Gender { get; set; }
        
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "*")]
        //[Remote("IsEmailAddressAvailable", "SignUp")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneOne { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneTwo { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string AddressOne { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string AddressTwo { get; set; }

        [StringLength(50)]
        public string AddressThree { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string TownCity { get; set; }

        [StringLength(50)]
        public string County { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "*")]
        public string Country { get; set; }

        [StringLength(7)]
        [DataType(DataType.PostalCode)]
        [Required(ErrorMessage = "*")]
        public string PostCode { get; set; }


        public List<SelectListItem> GenderList { get; set; }

        public SignUpModel()
        {
            GenderList = new List<SelectListItem>
            {
                new SelectListItem() { Value = "male", Text = "Male" },
                new SelectListItem() { Value = "female", Text = "Female" }
            };
        }
    }
}
