﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.Web.Models.AttributeValidation
{
    public class CustomDateAttribute : RangeAttribute
    {
        // Validates that users DOB is no more than 120 years old, and is not in the future
        public CustomDateAttribute() : base(typeof(DateTime), DateTime.Now.AddYears(-120).ToShortDateString(), DateTime.Now.ToShortDateString())
        {

        }
    }
}
