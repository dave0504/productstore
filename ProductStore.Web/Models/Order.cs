﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class Order
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime DateOrderPlaced { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime DateOrderPaid { get; set; }

        [DataType(DataType.Currency)]
        [Required]
        public decimal OrderTotal { get; set; }

        [DataType(DataType.Currency)]
        [Required]
        public decimal AmountPaid { get; set; }

        public int DeliveryID { get; set; }
        public ICollection<Delivery> Delivery { get; set; }

        [StringLength(250)]
        public string Comments { get; set; }

        public Customer Customer { get; set; }
        public ICollection<OrderItem> OrderItem { get; set; }
        public Payment Payment { get; set; }

    }
}
