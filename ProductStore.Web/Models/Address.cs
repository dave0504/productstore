﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Models
{
    public class Address
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string AddressOne { get; set; }

        [StringLength(50)]
        [Required]
        public string AddressTwo { get; set; }

        [StringLength(50)]
        public string AddressThree { get; set; }

        [StringLength(50)]
        [Required]
        public string TownCity { get; set; }

        [StringLength(50)]
        [Required]
        public string County { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        [Required]
        public string Country { get; set; }

        [StringLength(7)]
        [DataType(DataType.PostalCode)]
        [Required]
        public string PostCode { get; set; }

        public ICollection<CustomerAddresses> CustomerAddresses { get; set; }
        public ICollection<SupplierAddresses> SupplierAddresses { get; set; }
    }
}
