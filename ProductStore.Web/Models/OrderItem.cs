﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductStore.Models
{
    public class OrderItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductID { get; set; }

        [Required]
        public int Quantity { get; set; }

        [StringLength(250)]
        public string Comments { get; set; }

        public int? DeliveryID { get; set; }

        public Order Order { get; set; }
        public Delivery Delivery { get; set; }
        public Product Product { get; set; }
        
    }
}