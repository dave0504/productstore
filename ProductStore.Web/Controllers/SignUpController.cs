﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using System.Web.Mvc;
using ProductStore.Web.DAL;
using ProductStore.Web.Models;
using ProductStore.Models;
using ProductStore.DAL.Repositories;
using ProductStore.Repositories.Interfaces;
using ProductStore.Web.UnitsOfWork;

namespace ProductStore.Web.Controllers
{
    public class SignUpController : Controller
    {
        private DatabaseContext _context;
        private AccountRepository _acountRepository = UnitOfWork();

        public ActionResult Index()
        {
            return View(new SignUpModel());
        }

        [HttpPost]
        public ActionResult SignUp(SignUpModel SignUpModel)
        {

            if (ModelState.IsValid)
            {
                Customer customer = new Customer
                {
                    Firstname = SignUpModel.Firstname,
                    MiddleName = SignUpModel.MiddleName,
                    Surname = SignUpModel.Surname,
                    DOB = SignUpModel.DOB,
                    Gender = SignUpModel.Gender,
                    PhoneOne = SignUpModel.PhoneOne,
                    PhoneTwo = SignUpModel.PhoneTwo
                };

                Account account = new Account
                {
                    Username = SignUpModel.Username,
                    Email = SignUpModel.Email,
                    RegistrationDate = DateTime.Now.Date,
                    Password = SignUpModel.Password,
                    AccountStatus = AccountStatus.Active,
                    Customer = customer,
                    CustomerID = customer.ID
                };

                Address address = new Address
                {
                    AddressOne = SignUpModel.AddressOne,
                    AddressTwo = SignUpModel.AddressTwo,
                    AddressThree = SignUpModel.AddressThree,
                    TownCity = SignUpModel.TownCity,
                    County = SignUpModel.County,
                    State = SignUpModel.State,
                    PostCode = SignUpModel.PostCode,
                    Country = SignUpModel.Country,

                };
                ManageNewCustomerRegistration manageCustomerUnitOfWork = new ManageNewCustomerRegistration(_context);
                bool result = manageCustomerUnitOfWork.ProcessNewCustomerSignUp(customer, address, account);

                if(result)
                {
                    return RedirectToAction("Success");
                }
                else
                {
                    return RedirectToAction("Failed");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Failed()
        {
            return View();
        }


        public JsonResult IsEmailAddressAvailable(string email)
        {
            _context = new DatabaseContext();
            _acountRepository = new AccountRepository(_context);
            Account account = _acountRepository.FindAccountByEmailAddress(email);
                return Json(account == null ?
            "true" : string.Format("An account with that email address already exists."));
        }

        public JsonResult IsUsernameAvailable(string username)
        {
            _context = new DatabaseContext();
            _acountRepository = new AccountRepository(_context);
            Account account = _acountRepository.FindAccountByUsername(username);
            return Json(account == null ?
            "true" : string.Format("The username {0} has already been taken.", username));
        }
    }
}