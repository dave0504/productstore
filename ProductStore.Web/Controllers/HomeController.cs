﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using System.Web.Mvc;
using ProductStore.Web.DAL;

namespace ProductStore.Web.Controllers
{
    public class HomeController : Controller
    {

        private DatabaseContext _databaseContext = new DatabaseContext();

        public HomeController()
        {
            
        }

        public ActionResult Index()
        {
            //var suppliers = _databaseContext.Supplier.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
    }
}
